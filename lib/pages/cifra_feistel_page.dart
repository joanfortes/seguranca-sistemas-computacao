import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CifraFeistelPage extends StatefulWidget {
  static const routeName = '/cifra_feistel';
  const CifraFeistelPage({super.key});

  @override
  State<CifraFeistelPage> createState() => _CifraFeistelPageState();
}

class _CifraFeistelPageState extends State<CifraFeistelPage> {
  final TextEditingController _controladorPalavra = TextEditingController();
  final TextEditingController _controladorChaveInicial =
      TextEditingController();
  String _resultado = '';
  String _funcOperador = 'OR';
  final List<String> operadores = ['OR', 'AND'];
  final List<String> chaves = <String>[
    '1110',
    '0100',
    '1101',
    '0001',
    '0010',
    '1111',
    '1011',
    '1000',
    '0011',
    '1010',
    '0110',
    '1100',
    '0101',
    '1001',
    '0000',
    '0111',
  ];

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<String>> dropdownItens = [];
    for (var e in operadores) {
      dropdownItens.add(
        DropdownMenuItem<String>(
          value: e,
          child: Text(e),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cifra de Feistel'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: const InputDecoration(
                  hintText: 'Escreva seu texto aqui.',
                ),
                controller: _controladorPalavra,
                keyboardType: TextInputType.text,
              ),
              Container(height: 32.0),
              DropdownButton<String>(
                alignment: Alignment.center,
                isExpanded: true,
                items: dropdownItens,
                onChanged: (value) => setState(() {
                  _funcOperador = value!;
                }),
                value: _funcOperador,
              ),
              Container(height: 32.0),
              TextField(
                decoration: const InputDecoration(
                    hintText: 'Escreva sua chave inicial aqui.'),
                controller: _controladorChaveInicial,
                keyboardType: TextInputType.number,
              ),
              Container(height: 32.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  TextButton(
                    onPressed: () {
                      encriptar(_controladorPalavra.text);
                      setState(() {});
                    },
                    child: const Text('Encriptar'),
                  ),
                  TextButton(
                    child: const Text('Decriptar'),
                    onPressed: () {
                      decriptar(_controladorPalavra.text);
                      setState(() {});
                    },
                  ),
                  TextButton(
                    onPressed: () {
                      _controladorChaveInicial.clear();
                      _controladorPalavra.clear();
                      _resultado = '';
                      setState(() {});
                    },
                    child: const Text('Deletar'),
                  ),
                ],
              ),
              Container(height: 64.0),
              const Text(
                'Resultado :',
                style: TextStyle(fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
              Container(height: 32.0),
              SelectableText(
                _resultado,
                style: const TextStyle(fontSize: 32.0),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String encriptar(String message) {
    int meioMensagem = (message.length / 2).floor();
    String esquerda = message.substring(0, meioMensagem);
    String direita = message.substring(meioMensagem);

    for (int indexArredondado = 0; indexArredondado < 16; indexArredondado++) {
      String temp = direita;
      String functionText = funcao(direita, indexArredondado);
      direita = xor(esquerda, functionText);
      esquerda = temp;
    }
    return _resultado = "$esquerda$direita";
  }

  String decriptar(String mensagemEncriptada) {
    int messageMid = (mensagemEncriptada.length / 2).floor();
    String esquerda = mensagemEncriptada.substring(0, messageMid);
    String direita = mensagemEncriptada.substring(messageMid);

    for (int indexArredondado = 0; indexArredondado < 16; indexArredondado++) {
      String temp = esquerda;
      String functionText = funcao(esquerda, 16 - indexArredondado - 1);
      esquerda = xor(direita, functionText);
      direita = temp;
    }

    return _resultado = "$esquerda$direita";
  }

  String funcao(String esquerda, int indexArredondado) {
    String currentKey = getSubChave(indexArredondado);
    String encryptedText = "";

    switch (_funcOperador) {
      case "AND":
        encryptedText = and(esquerda, currentKey);
        break;
      case "OR":
        encryptedText = or(esquerda, currentKey);
        break;
    }
    return encryptedText;
  }

  String getSubChave(int indexArredondado) {
    int x = (int.parse(_controladorChaveInicial.text) + indexArredondado) % 16;

    return chaves.elementAt(x);
  }

  String xor(String esquerda, String direita) {
    Uint8List esquerdaBits = Uint8List.fromList(esquerda.codeUnits);
    Uint8List direitaBits = Uint8List.fromList(direita.codeUnits);

    if (esquerdaBits.lengthInBytes == 0 || direitaBits.lengthInBytes == 0) {
      throw ArgumentError.value(
          "lengthInBytes of Uint8List arguments must be > 0");
    }

    bool aIsBigger = esquerdaBits.lengthInBytes > direitaBits.lengthInBytes;
    int length =
        aIsBigger ? esquerdaBits.lengthInBytes : direitaBits.lengthInBytes;

    Uint8List buffer = Uint8List(length);

    for (int i = 0; i < length; i++) {
      int aa, bb;
      try {
        aa = esquerdaBits.elementAt(i);
      } catch (e) {
        aa = 0;
      }
      try {
        bb = direitaBits.elementAt(i);
      } catch (e) {
        bb = 0;
      }

      buffer[i] = aa ^ bb;
    }

    return String.fromCharCodes(buffer);
  }

  String or(String esquerda, String direita) {
    Uint8List esquerdaBits = Uint8List.fromList(esquerda.codeUnits);
    Uint8List direitaBits = Uint8List.fromList(direita.codeUnits);

    if (esquerdaBits.lengthInBytes == 0 || direitaBits.lengthInBytes == 0) {
      throw ArgumentError.value(
          "lengthInBytes of Uint8List arguments must be > 0");
    }

    bool aIsBigger = esquerdaBits.lengthInBytes > direitaBits.lengthInBytes;
    int length =
        aIsBigger ? esquerdaBits.lengthInBytes : direitaBits.lengthInBytes;

    Uint8List buffer = Uint8List(length);

    for (int i = 0; i < length; i++) {
      int aa, bb;
      try {
        aa = esquerdaBits.elementAt(i);
      } catch (e) {
        aa = 0;
      }
      try {
        bb = direitaBits.elementAt(i);
      } catch (e) {
        bb = 0;
      }

      buffer[i] = aa | bb;
    }

    return String.fromCharCodes(buffer);
  }

  String and(String esquerda, String direita) {
    Uint8List esquerdaBits = Uint8List.fromList(esquerda.codeUnits);
    Uint8List direitaBits = Uint8List.fromList(direita.codeUnits);

    if (esquerdaBits.lengthInBytes == 0 || direitaBits.lengthInBytes == 0) {
      throw ArgumentError.value(
          "lengthInBytes of Uint8List arguments must be > 0");
    }

    bool aIsBigger = esquerdaBits.lengthInBytes > direitaBits.lengthInBytes;
    int length =
        aIsBigger ? esquerdaBits.lengthInBytes : direitaBits.lengthInBytes;

    Uint8List buffer = Uint8List(length);

    for (int i = 0; i < length; i++) {
      int aa, bb;
      try {
        aa = esquerdaBits.elementAt(i);
      } catch (e) {
        aa = 0;
      }
      try {
        bb = direitaBits.elementAt(i);
      } catch (e) {
        bb = 0;
      }

      buffer[i] = aa & bb;
    }

    return String.fromCharCodes(buffer);
  }
}
