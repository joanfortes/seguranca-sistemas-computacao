import 'package:encrypt/encrypt.dart';
import 'package:exercicio_cifra_cesar/utils/funcoes_uteis.dart';
import 'package:flutter/material.dart' hide Key;

class AESPage extends StatefulWidget {
  static const routeName = '/aes_page';
  const AESPage({super.key});

  @override
  State<AESPage> createState() => _AESPageState();
}

class _AESPageState extends State<AESPage> {
  final TextEditingController _controladorPalavra = TextEditingController();
  final TextEditingController _controladorChave = TextEditingController();
  String mode = 'cbc';
  String _resultado = '';
  Encrypted? dadoEncriptado;
  @override
  void dispose() {
    _controladorPalavra.dispose();
    _controladorChave.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Criptografia AES'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: const InputDecoration(
                  hintText: 'Escreva seu texto aqui.',
                ),
                controller: _controladorPalavra,
                keyboardType: TextInputType.text,
              ),
              Container(
                height: 32.0,
              ),
              DropdownButton<String>(
                alignment: Alignment.center,
                isExpanded: true,
                items: const [
                  DropdownMenuItem<String>(
                    value: 'cbc',
                    child: Text('cbc'),
                  ),
                  DropdownMenuItem<String>(
                    value: 'cesar',
                    child: Text('cifra de cesar'),
                  ),
                ],
                onChanged: (value) => setState(() {
                  mode = value!;
                }),
                value: mode,
              ),
              Container(
                height: 32.0,
              ),
              Visibility(
                visible: mode == 'cesar',
                child: TextField(
                  decoration: const InputDecoration(
                      hintText: 'Escreva sua chave aqui.'),
                  controller: _controladorChave,
                  keyboardType: TextInputType.number,
                ),
              ),
              Container(
                height: 32.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  TextButton(
                    onPressed: () =>
                        mode == 'cbc' ? _encriptar() : _processo(true),
                    child: const Text('Encriptar'),
                  ),
                  TextButton(
                    child: const Text('Decriptar'),
                    onPressed: () =>
                        mode == 'cbc' ? _decriptar() : _processo(false),
                  ),
                  TextButton(
                    onPressed: _deletar,
                    child: const Text('Deletar'),
                  ),
                ],
              ),
              Container(
                height: 64.0,
              ),
              const Text(
                'Resultado :',
                style: TextStyle(fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
              Container(
                height: 32.0,
              ),
              SelectableText(
                _resultado,
                style: const TextStyle(fontSize: 32.0),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _encriptar() {
    try {
      dadoEncriptado = Encrypter(AES(Key.fromLength(32), mode: AESMode.cbc))
          .encrypt(_controladorPalavra.text, iv: IV.fromLength(16));
    } catch (e) {
      mostrarAlerta(e.toString(), context);
    }

    setState(() {
      if (dadoEncriptado != null) {
        _resultado = dadoEncriptado!.base64;
      } else {
        _resultado = '';
      }
    });
  }

  void _decriptar() {
    if (dadoEncriptado != null) {
      setState(() {
        _resultado = Encrypter(AES(Key.fromLength(32), mode: AESMode.cbc))
            .decrypt(dadoEncriptado!, iv: IV.fromLength(16));
      });
    } else {
      mostrarAlerta('não tem dado encripitado para decriptar', context);
    }
  }

  //funçao que pega a palavra e executa a criptografia
  void _processo(bool eEncriptado) {
    String text = _controladorPalavra.text.toLowerCase();
    //chave usada para encriptar
    int chave = 0;
    String temp = '';

    try {
      chave = int.parse(_controladorChave.text);
    } catch (e) {
      mostrarAlerta('Chave inválida', context);
    }

    //essa estrutura de repetiçao garante que codigo de cada letra seja correta de acordo com UTF-16
    for (int i = 0; i < text.length; i++) {
      int ch = text.codeUnitAt(i);
      //offset é o deslocamento entre o codigo numerico e o código ASCII
      int offset;
      String h;
      if (ch >= 'a'.codeUnitAt(0) && ch <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch >= 'A'.codeUnitAt(0) && ch <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      } else if (ch == ' '.codeUnitAt(0)) {
        temp += ' ';
        continue;
      } else {
        mostrarAlerta('Texto inválido', context);
        temp = '';
        break;
      }

      //aqui o caracter é encriptado ou descriptado
      int? c;
      //garante, caso o valor seja maior que 26, que a criptografia seja igual. por exemplo: chave 1 = chave 27
      if (eEncriptado) {
        c = ((ch + chave - offset) % 26);
      } else {
        c = ((ch - chave - offset) % 26);
      }
      h = String.fromCharCode(c + offset);
      temp += h;
    }

    setState(() {
      _resultado = temp;
    });
  }

  void _deletar() {
    _controladorPalavra.clear();
    _controladorChave.clear();
    setState(() {
      _resultado = '';
    });
  }
}
