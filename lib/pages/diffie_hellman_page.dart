import 'package:flutter/material.dart';
import 'package:pinenacl/x25519.dart';

class DiffieHellmanPage extends StatefulWidget {
  static const routeName = '/diffie_hellman';
  const DiffieHellmanPage({super.key});

  @override
  State<DiffieHellmanPage> createState() => _DiffieHellmanPageState();
}

class _DiffieHellmanPageState extends State<DiffieHellmanPage> {
  final TextEditingController _controladorPalavra = TextEditingController();
  final p1ChaveSecreta = PrivateKey.generate();
  final p2SecretKey = PrivateKey.generate();
  String _resultado = '';
  EncryptedMessage? dadoEncriptado;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final p1ChavePublica = p1ChaveSecreta.publicKey;
    final p2ChavePublica = p2SecretKey.publicKey;
    final p1Box =
        Box(myPrivateKey: p1ChaveSecreta, theirPublicKey: p2ChavePublica);
    final p2Box =
        Box(myPrivateKey: p2SecretKey, theirPublicKey: p1ChavePublica);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Diffie Hellman'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('Chave pública pessoa 1: $p1ChavePublica',
                    style: theme.primaryTextTheme.bodyLarge!
                        .copyWith(color: Colors.black)),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('Chave pública pessoa 2: $p2ChavePublica',
                    textAlign: TextAlign.start,
                    style: theme.primaryTextTheme.bodyLarge!
                        .copyWith(color: Colors.black)),
              ),
              TextField(
                decoration: const InputDecoration(
                  hintText: 'Escreva seu texto aqui.',
                ),
                controller: _controladorPalavra,
                keyboardType: TextInputType.text,
              ),
              Container(height: 32.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  TextButton(
                    onPressed: () {
                      dadoEncriptado = p1Box.encrypt(Uint8List.fromList(
                          _controladorPalavra.text.codeUnits));
                      _resultado =
                          String.fromCharCodes(dadoEncriptado!.cipherText);
                      setState(() {});
                    },
                    child: const Text('Encriptar'),
                  ),
                  TextButton(
                    child: const Text('Decriptar'),
                    onPressed: () {
                      _resultado =
                          String.fromCharCodes(p2Box.decrypt(dadoEncriptado!));
                      setState(() {});
                    },
                  ),
                  TextButton(
                    onPressed: () {
                      _controladorPalavra.clear();
                      _resultado = '';
                      setState(() {});
                    },
                    child: const Text('Deletar'),
                  ),
                ],
              ),
              Container(height: 64.0),
              const Text(
                'Resultado :',
                style: TextStyle(fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
              Container(height: 32.0),
              SelectableText(
                _resultado,
                style: const TextStyle(fontSize: 32.0),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
