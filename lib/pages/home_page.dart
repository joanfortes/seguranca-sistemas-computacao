import 'package:exercicio_cifra_cesar/pages/aes_page.dart';
import 'package:exercicio_cifra_cesar/pages/cifra_cesar_page.dart';
import 'package:exercicio_cifra_cesar/pages/cifra_feistel_page.dart';
import 'package:exercicio_cifra_cesar/pages/diffie_hellman_page.dart';
import 'package:exercicio_cifra_cesar/utils/tile.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/home';
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Telas com codigo implementado dos exercicios de segurança de sistemas de computação.',
                style: theme.textTheme.headline6,
              ),
              const SizedBox(
                height: 12,
              ),
              GridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12,
                mainAxisSpacing: 12,
                shrinkWrap: true,
                children: [
                  buildTile(
                    context,
                    theme,
                    CifraCesar.routeName,
                    Icons.computer,
                    'Cifra de Cesar',
                  ),
                  buildTile(
                    context,
                    theme,
                    CifraFeistelPage.routeName,
                    Icons.align_horizontal_left_rounded,
                    'Cifra de feistel',
                  ),
                  buildTile(
                    context,
                    theme,
                    AESPage.routeName,
                    Icons.abc_outlined,
                    'Criptografia AES',
                  ),
                  buildTile(
                    context,
                    theme,
                    DiffieHellmanPage.routeName,
                    Icons.mail,
                    'Diffie Hellman',
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
