import 'package:exercicio_cifra_cesar/utils/funcoes_uteis.dart';
import 'package:flutter/material.dart';
import 'package:string_stats/string_stats.dart';

class CifraCesar extends StatefulWidget {
  static const routeName = '/cifra_cesar';
  const CifraCesar({super.key});

  @override
  State<CifraCesar> createState() => _CifraCesarState();
}

class _CifraCesarState extends State<CifraCesar> {
  final TextEditingController _controladorPalavra = TextEditingController();
  final TextEditingController _controladorChave = TextEditingController();
  String _resultado = '';
  final List<TableRow> _ocorrencias = <TableRow>[];

  @override
  void dispose() {
    _controladorPalavra.dispose();
    _controladorChave.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cifra de Cesar'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: const InputDecoration(
                  hintText: 'Escreva seu texto aqui.',
                ),
                controller: _controladorPalavra,
                keyboardType: TextInputType.text,
              ),
              Container(
                height: 32.0,
              ),
              TextField(
                decoration:
                    const InputDecoration(hintText: 'Escreva sua chave aqui.'),
                controller: _controladorChave,
                keyboardType: TextInputType.number,
              ),
              Container(
                height: 32.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  TextButton(
                    onPressed: () => _processo(true, themeData: theme),
                    child: const Text('Encriptar'),
                  ),
                  TextButton(
                    child: const Text('Decriptar'),
                    onPressed: () => _processo(false, themeData: theme),
                  ),
                  TextButton(
                    onPressed: _deletar,
                    child: const Text('Deletar'),
                  ),
                ],
              ),
              Container(
                height: 64.0,
              ),
              const Text(
                'Resultado :',
                style: TextStyle(fontSize: 24.0),
                textAlign: TextAlign.center,
              ),
              Container(
                height: 32.0,
              ),
              SelectableText(
                _resultado,
                style: const TextStyle(fontSize: 32.0),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              Table(
                border: TableBorder.all(color: Colors.black),
                children: _ocorrencias,
              )
            ],
          ),
        ),
      ),
    );
  }

  //funçao que pega a palavra e executa a criptografia
  void _processo(bool eEncriptado, {required ThemeData themeData}) {
    String text = _controladorPalavra.text.toLowerCase();
    //chave usada para encriptar
    int chave = 0;
    String temp = '';
    _ocorrencias.clear();

    try {
      chave = int.parse(_controladorChave.text);
    } catch (e) {
      mostrarAlerta('Chave inválida', context);
    }

    //essa estrutura de repetiçao garante que codigo de cada letra seja correta de acordo com UTF-16
    for (int i = 0; i < text.length; i++) {
      int ch = text.codeUnitAt(i);
      //offset é o deslocamento entre o codigo numerico e o código ASCII
      int offset;
      String h;
      if (ch >= 'a'.codeUnitAt(0) && ch <= 'z'.codeUnitAt(0)) {
        offset = 97;
      } else if (ch >= 'A'.codeUnitAt(0) && ch <= 'Z'.codeUnitAt(0)) {
        offset = 65;
      } else if (ch == ' '.codeUnitAt(0)) {
        temp += ' ';
        continue;
      } else {
        mostrarAlerta('Texto inválido', context);
        temp = '';
        break;
      }

      //aqui o caracter é encriptado ou descriptado
      int? c;
      //garante, caso o valor seja maior que 26, que a criptografia seja igual. por exemplo: chave 1 = chave 27
      if (eEncriptado) {
        c = ((ch + chave - offset) % 26);
      } else {
        c = ((ch - chave - offset) % 26);
      }
      h = String.fromCharCode(c + offset);
      temp += h;
    }

    var letters = temp.characters;
    for (var caractere in letters.toSet()) {
      _ocorrencias.add(
        TableRow(
          children: [
            Text(
              caractere.toUpperCase(),
              textAlign: TextAlign.center,
              style: themeData.textTheme.headlineLarge!
                  .copyWith(fontWeight: FontWeight.bold, color: Colors.black),
            ),
            Text(
              '${((allCharFrequency(temp)[caractere]! / temp.length) * 100).toStringAsFixed(2).toString()}%',
              textAlign: TextAlign.center,
              style: themeData.textTheme.headlineLarge!
                  .copyWith(fontWeight: FontWeight.bold, color: Colors.black),
            ),
          ],
        ),
      );
    }

    setState(() {
      _resultado = temp;
    });
  }

  void _deletar() {
    _controladorPalavra.clear();
    _controladorChave.clear();
    _ocorrencias.clear();
    setState(() {
      _resultado = '';
    });
  }
}
