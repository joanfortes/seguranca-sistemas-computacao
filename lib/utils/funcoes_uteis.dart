import 'package:flutter/material.dart';

Future<void> mostrarAlerta(String alert, BuildContext context) async {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Ocorreu um erro'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(alert),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
