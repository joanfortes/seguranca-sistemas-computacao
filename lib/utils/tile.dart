import 'package:flutter/material.dart';

GestureDetector buildTile(
  BuildContext context,
  ThemeData theme,
  String routeName,
  IconData icon,
  String title,
) {
  return GestureDetector(
    onTap: () => Navigator.of(context).pushNamed(routeName),
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(),
        color: theme.secondaryHeaderColor,
        borderRadius: const BorderRadius.all(Radius.circular(12)),
      ),
      child: Column(
        children: [
          Flexible(
            flex: 4,
            child: Center(
              child: Icon(icon, size: 60),
            ),
          ),
          Flexible(
            child: Text(
              title,
              style: theme.textTheme.headline6,
            ),
          )
        ],
      ),
    ),
  );
}
