import 'package:flutter/material.dart';

class SlideRoute extends PageRouteBuilder {
  final Widget? page;
  final String mode;

  SlideRoute({this.page, this.mode = 'left', super.settings})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          transitionDuration: const Duration(milliseconds: 350),
          reverseTransitionDuration: const Duration(milliseconds: 350),
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            Offset localMode;

            switch (mode) {
              case 'right':
                localMode = const Offset(-1, 0);
                break;
              case 'top':
                localMode = const Offset(0, 1);
                break;
              case 'bottom':
                localMode = const Offset(0, -1);
                break;
              default:
                localMode = const Offset(1, 0); // left
            }

            return SlideTransition(
              position: Tween<Offset>(
                begin: localMode,
                end: Offset.zero,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Curves.ease,
                ),
              ),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      spreadRadius: 5,
                      blurRadius: 12,
                      offset: const Offset(0, 8),
                    ),
                  ],
                ),
                child: child,
              ),
            );
          },
        );
}
