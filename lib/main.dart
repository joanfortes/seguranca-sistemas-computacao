import 'package:exercicio_cifra_cesar/pages/aes_page.dart';
import 'package:exercicio_cifra_cesar/pages/cifra_cesar_page.dart';
import 'package:exercicio_cifra_cesar/pages/cifra_feistel_page.dart';
import 'package:exercicio_cifra_cesar/pages/diffie_hellman_page.dart';
import 'package:exercicio_cifra_cesar/pages/home_page.dart';
import 'package:flutter/material.dart';

import 'utils/slide_route.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Segurança em sistemas de informação',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case HomePage.routeName:
            return SlideRoute(page: const HomePage());
          case CifraCesar.routeName:
            return SlideRoute(page: const CifraCesar());
          case CifraFeistelPage.routeName:
            return SlideRoute(page: const CifraFeistelPage());
          case AESPage.routeName:
            return SlideRoute(page: const AESPage());
          case DiffieHellmanPage.routeName:
            return SlideRoute(page: const DiffieHellmanPage());
          default:
            return SlideRoute(page: const HomePage());
        }
      },
      home: const HomePage(),
    );
  }
}
