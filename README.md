# exercicios segurança sistemas de computação

projeto com as listas de exercicios semanais do curso.

## Para Rodar

rode o comando `flutter pub get` e selecione um dos modos no `run and debug`do vscode.

links úteis:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
